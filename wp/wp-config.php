<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'sassland_git' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '^I4hqj@7iF*DKl2niMY(e|X+DfVIX5!]mWSIAxX4>8E[$7}QPrp67yN3WS:/vb5o' );
define( 'SECURE_AUTH_KEY',  'Y78lRh_kkr-&h/Fh}g3^J_6ZO+p|E9DqD2t&iMkhaEFD[HGpe@aI~N0K}QH6jbJ*' );
define( 'LOGGED_IN_KEY',    'U{jn!6G.rV8<u4{IYah?&%=k2sz 7%81n$*!mC^7m>&DF2kNKE%]Pn7SOiS[2}bV' );
define( 'NONCE_KEY',        'm]By+VHz==aBuEBi<. %y>`=<Lxi@HR+Nlqo5^8E;{,)|`^CLn?fD5RoJE3+]D*(' );
define( 'AUTH_SALT',        ':hN1v+CJDOSUIwI!?k*L=aU~n-?uY(X!eq^1Hw}<MtW 6tIcFh+u[UcX5UdkUsEk' );
define( 'SECURE_AUTH_SALT', 'NPO%rid!fEZpsS3-{eCAIz(Xx&2G,$G1|#*]GDB&ek6^5Sp#6Gm|_&|nq4&G?58u' );
define( 'LOGGED_IN_SALT',   'LY(ZJ2eL>98+d}>4,Px7fA%N3OSemJo*3B+&<@]zX6buw!e8Bn%Yan7M]aTaBU]6' );
define( 'NONCE_SALT',       'e[TS%={wNNqZ*Z9OEM>+m&h_=6Uo:jgKYQ<5G;X>!vd!u,8.!TzoNMBsuNK5LVlU' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

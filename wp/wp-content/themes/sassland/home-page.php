<?php
/* Template Name: Front Page */

get_header();
?>
    <?php include('components/pre-loading.php'); ?>
    
    <div class="body_wrapper">
       
       <?php include('components/header-menu.php'); ?>

        <section class="digital_banner_area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-8">
                        <div class="digital_content">
                            <h2>Get Some Fresh</h2>
                            <p>Nice one mufty brown bread James Bond lost the plot chinwag vagabond are you taking the piss morish matie boy bender, spend a penny blower cockup easy.!</p>
                            <a href="#" class="btn_six slider_btn">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="digital_video_slider owl-carousel">
                <div class="video_item">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/new/video_img_01.png" alt="">
                    <a class="popup-youtube video_icon" href="https://www.youtube.com/watch?v=sU3FkzUKHXU"><i class="arrow_triangle-right"></i></a>
                </div>
                <div class="video_item">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/new/video_img_02.jpg" alt="">
                    <a class="popup-youtube video_icon" href="https://www.youtube.com/watch?v=sU3FkzUKHXU"><i class="arrow_triangle-right"></i></a>
                </div>
            </div>
            <div class="digital_banner_shap"></div>
            <div class="round_shap one"></div>
            <div class="round_shap two"></div>
            <div class="round_shap three"></div>
        </section>
        <section class="partner_logo_area_six bg_color">
            <div class="container">
                <div class="row partner_info">
                    <div class="logo_item wow fadeInLeft" data-wow-delay="0.2s" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_01.png" alt=""></a>
                    </div>
                    <div class="logo_item wow fadeInLeft" data-wow-delay="0.4s" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_02.png" alt=""></a>
                    </div>
                    <div class="logo_item wow fadeInLeft" data-wow-delay="0.6s" style="visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_03.png" alt=""></a>
                    </div>
                    <div class="logo_item wow fadeInLeft" data-wow-delay="0.8s" style="visibility: visible; animation-delay: 0.8s; animation-name: fadeInLeft;">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_04.png" alt=""></a>
                    </div>
                    <div class="logo_item wow fadeInLeft" data-wow-delay="0.9s" style="visibility: visible; animation-delay: 0.9s; animation-name: fadeInLeft;">
                        <a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/new/logo_05.png" alt=""></a>
                    </div>
                </div>
            </div>
        </section>
        <section class="startup_fuatures_area_two sec_pad">
            <div class="container">
                <div class="sec_title mb_70 wow fadeInUp" data-wow-delay="0.4s">
                    <h2 class="f_p f_size_30 l_height40 f_600 t_color text-center">Ways your team can<br> Use hubstaff</h2>
                </div>
                <ul class="nav nav-tabs startup_tab">
                    <li class="nav-item">
                        <a class="nav-link" href="#market">
                            <span class="icon"><i class="icon-cloud-upload"></i></span>
                            <h3>Marketing<br> Cloud</h3>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#app">
                            <span class="icon"><i class="icon-screen-tablet"></i></span>
                            <h3>Commerce<br> Apps</h3>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#hubstaff">
                            <span class="icon"><i class="icon-graduation"></i></span>
                            <h3>Hubstaff<br> App</h3>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#engine">
                            <span class="icon"><i class="icon-refresh"></i></span>
                            <h3>Automation<br> Engine</h3>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#business">
                            <span class="icon"><i class="icon-bulb"></i></span>
                            <h3>Business<br> Intellegence</h3>
                        </a>
                    </li>
                </ul>
            </div>
        </section>
        <section class="video_area bg_color sec_pad">
            <div class="container">
                <div class="sec_title text-center mb_70">
                    <h2 class="f_p f_size_30 l_height45 f_600 t_color3">The SaasLand platform</h2>
                    <p class="f_400 f_size_16 mb-0">Samsa will only charge a fee if you make a profit</p>
                </div>
                <div class="video_content">
                    <div class="video_info">
                        <div class="ovarlay_color"></div>
                        <a class="popup-youtube video_icon" href="https://www.youtube.com/watch?v=sU3FkzUKHXU"><i class="arrow_triangle-right"></i></a>
                        <h2>SaasLand</h2>
                    </div>
                    <img class="video_leaf" src="<?php echo get_template_directory_uri(); ?>/img/new/leaf.png" alt="">
                    <img class="cup" src="<?php echo get_template_directory_uri(); ?>/img/new/cup.png" alt="">
                </div>
            </div>
        </section>
        <section class="portfolio_area sec_pad">
            <div class="container">
                <div class="sec_title mb_70 wow fadeInUp" data-wow-delay="0.4s">
                    <h2 class="f_p f_size_30 l_height40 f_600 t_color text-center">take a look around<br>our portfolio</h2>
                </div>
                <div id="portfolio_filter" class="portfolio_filter portfolio_filter_blue mb_50">
                    <div data-filter="*" class="work_portfolio_item active">See All</div>
                    <div data-filter=".br" class="work_portfolio_item">Branding</div>
                    <div data-filter=".web" class="work_portfolio_item">Web Design</div>
                    <div data-filter=".fashion" class="work_portfolio_item">Fashion</div>
                    <div data-filter=".ux" class="work_portfolio_item">UI/UX</div>
                    <div data-filter=".product" class="work_portfolio_item">Product</div>
                </div>
                <div class="row portfolio_gallery mb_30" id="work-portfolio">
                    <div class="col-lg-4 col-sm-6 portfolio_item br ux mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/3_col_1.jpg" alt="">
                            <div class="hover_content h_content_two">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/3_col_1.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Apple Mobile Mockup</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 portfolio_item fashion web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/3_col_2.jpg" alt="">
                            <div class="hover_content h_content_two">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/3_col_2.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Interior Design</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 portfolio_item product br mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/3_col_4.jpg" alt="">
                            <div class="hover_content h_content_two">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/3_col_4.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Portfolio Center Slider</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 portfolio_item ux web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/3_col_5.jpg" alt="">
                            <div class="hover_content h_content_two">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/3_col_5.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Portfolio Masonry</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 portfolio_item br ux mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/3_col_6.jpg" alt="">
                            <div class="hover_content h_content_two">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/3_col_6.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Double Exposure</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6 portfolio_item fashion web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/3_col_7.jpg" alt="">
                            <div class="hover_content h_content_two">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/3_col_7.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">American Burger</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <a href="#" class="btn_six slider_btn pr_btn">View All Work</a>
                </div>
            </div>
        </section>
        <section class="testimonial_area_five sec_pad bg_color">
            <div class="testimonial_shap_img" style="background: url('<?php echo get_template_directory_uri(); ?>/img/new/testimonial_bg_shap.png') no-repeat bottom left;"></div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 d-flex align-items-center">
                        <div class="testimonial_title">
                            <h2 class="f_p f_size_30 l_height45 f_600 t_color">Loved by businesses,<br>and individuals across<br>the globe.</h2>
                        </div>
                    </div>
                    <div class="col-lg-6 offset-lg-2">
                        <div class="stratup_testimonial_info d-flex align-items-center">
                            <div class="testimonial_slider_four owl-carousel">
                                <div class="item">
                                    <div class="author_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/new/member_01.jpg" alt="">
                                    </div>
                                    <p>He nicked it hanky panky Eaton naff it's your round quaint cheeky cheers, tomfoolery bonnet posh blimey what a plonker vagabond, zonked Elizabeth give us a bell.?</p>
                                    <h5>Phillip Anthropy</h5>
                                    <h6>UI/UX designer</h6>
                                </div>
                                <div class="item">
                                    <div class="author_img">
                                        <img src="<?php echo get_template_directory_uri(); ?>/img/new/member_01.jpg" alt="">
                                    </div>
                                    <p>He nicked it hanky panky Eaton naff it's your round quaint cheeky cheers, tomfoolery bonnet posh blimey what a plonker vagabond, zonked Elizabeth give us a bell.?</p>
                                    <h5>Phillip Anthropy</h5>
                                    <h6>UI/UX designer</h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php include('components/footer-menu.php'); ?>

    </div>
  

    <?php
get_footer();
 <header class="header_area">
            <nav class="navbar navbar-expand-lg menu_two">
                <div class="container">
                    <a class="navbar-brand sticky_logo" href="<?php echo get_site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/img/main-logo.png" srcset="img/logo2x-2.png 2x" alt="logo"><img src="<?php echo get_template_directory_uri(); ?>/img/main-logo.png" srcset="img/logo2x.png 2x" alt=""></a>
                    <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="menu_toggle">
                            <span class="hamburger">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                            <span class="hamburger-cross">
                                <span></span>
                                <span></span>
                            </span>
                        </span>
                    </button>
                    <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                        <ul class="navbar-nav menu w_menu">
                            <li class="nav-item dropdown submenu mega_menu mega_menu_two custom-main-menu-wrapper">
                                <?php
                  wp_nav_menu( array(
                    'theme_location' => 'my-custom-menu',
                    'container_class' => 'custom-menu-class' ) );
                    ?>
                                <!-- <a class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Home
                                </a> -->
                            </li>
                            
                        </ul>
                    </div>
                
                    
                    <div class="nav_right_btn">
                        <a href="#" class="login_btn active">Sign Up</a>
                    </div>
                </div>
            </nav>
        </header>
<?php
/* Template Name: Portfolio Page */

get_header();
?>

    <?php include('components/pre-loading.php'); ?>
    
    <div class="body_wrapper">
        
        <?php include('components/header-menu.php'); ?>

        <section class="breadcrumb_area">
            <img class="breadcrumb_shap" src="<?php echo get_template_directory_uri(); ?>/img/breadcrumb/banner_bg.png" alt="">
            <div class="container">
                <div class="breadcrumb_content text-center">
                    <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Portfolio grid 2 column</h1>
                    <p class="f_400 w_color f_size_16 l_height26">Why I say old chap that is spiffing off his nut arse pear shaped plastered<br> Jeffrey bodge barney some dodgy.!!</p>
                </div>
            </div>
        </section>
        <section class="portfolio_area sec_pad bg_color">
            <div class="container">
                <div id="portfolio_filter" class="portfolio_filter mb_50">
                    <div data-filter="*" class="work_portfolio_item active">See All</div>
                    <div data-filter=".br" class="work_portfolio_item">Branding</div>
                    <div data-filter=".web" class="work_portfolio_item">Web Design</div>
                    <div data-filter=".fashion" class="work_portfolio_item">Fashion</div>
                    <div data-filter=".ux" class="work_portfolio_item">UI/UX</div>
                    <div data-filter=".product" class="work_portfolio_item">Product</div>
                </div>
                <div class="row portfolio_gallery mb_30" id="work-portfolio">
                    <div class="col-sm-6 portfolio_item br ux mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid1.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid1.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Apple Mobile Mockup</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item fashion web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid2.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid2.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Interior Design</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item product br mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid3.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid3.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Portfolio Center Slider</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item ux web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid4.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid4.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Portfolio Masonry</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item br ux mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid5.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid5.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Double Exposure</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item fashion web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid6.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid6.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">American Burger</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item product br mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid7.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid7.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Apple Laptop Mockup</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 portfolio_item ux web mb-30">
                        <div class="portfolio_img">
                            <img src="<?php echo get_template_directory_uri(); ?>/img/new/grid8.jpg" alt="">
                            <div class="hover_content">
                                <a href="<?php echo get_template_directory_uri(); ?>/img/new/grid8.jpg" class="img_popup leaf"><i class="ti-plus"></i></a>
                                <div class="portfolio-description leaf">
                                    <a href="work-default.html" class="portfolio-title">
                                        <h3 class="f_500 f_size_20 f_p">Inner Smart Watch</h3>
                                    </a>
                                    <div class="links"><a href="#">Branding,</a><a href="#">Fashion</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php include('components/footer-menu.php'); ?>
        
    </div>

  <?php
get_footer();